# Shared Memory

```sh
$ make
gcc -o svshm_string_write svshm_string_write.c
gcc -o svshm_string_read svshm_string_read.c

$ ./svshm_string_read
shmid = 98365; semid = 3
Hello, world!

$ ./svshm_string_write 98365 3 'Hello, world!'
```
